# Filters

Filters are a way to refine listing results based on one or more conditions.

### How to use

To add a filter you have to use the query param key `filters` with the following format: `filters[column-name][operator]=value`

#### Example: Fetching `shops` from California

`kushy.net/api/v3/shops/?filters[location_state][like]=ca`

`filters[location_state][eq]=us` filters the customers to only those with `location_state` equal to `ca`.

Also `filters[location_state][eq]=ca` can be simplified to `filters[location_state]=ca`

To add another filter you have to separate each filters with the ampersand symbol (`&`).

#### Example: Fetching `shops` from Los Angeles, California

`kushy.net/api/v3/shops/?filters[location_city][like]=los%20angeles`

#### Supported Operators

Operator                | Description
----------------------- | ----------------------
`=`, `eq`               | Equal to
`<>`, `!=`, `neq`       | Not Equal to
`<`, `lt`               | Less than
`<=`, `lte`             | Less than or equal to
`>`, `gt`               | Greater than
`>=`, `gte`             | Greater than or equal to
`contains`              | Contains a string
`ncontains`             | Not Contains a string
`between`               | Is Between
`nbetween`              | Is Not Between
`empty`                 | Is Empty
`nempty`                | Is Not Empty