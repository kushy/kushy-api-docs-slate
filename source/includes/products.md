# Products

## Get All Products

```shell
  curl "http://kushy.net/api/v3/products/"
```

```javascript
var api = 'http://kushy.net/api/v3/products/';
var xmlhttp;
xmlhttp = new XMLHttpRequest();
xmlhttp.onreadystatechange = function(){
    if (xmlhttp.readyState == 4 && xmlhttp.status == 200){
      products = JSON.parse(this.responseText);
      console.log(products);
    }
}
xmlhttp.open("GET", api, true);
xmlhttp.send();
```

```php
<?php
$api = 'http://api.kushy.net/api/1.1/tables/products/';
$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => $api,
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "GET",
  CURLOPT_HTTPHEADER => array(
    "cache-control: no-cache"
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);
$response = json_decode($response, true);
echo 'Product Name'. $response['data'][0]['name'];
?>
```

> The above command returns JSON structured like this:

```json
{
    meta: {
    table: "products",
    type: "collection",
    total: 20,
    total_entries: 0
},
data: [
    {
    id: 1,
    name: "Pre-Roll Package - Pre-roll",
    slug: "the-humboldt-cure-pre-roll-package-pre-roll",
    brand: "The Humboldt Cure",
    category: "Flowers,Pre-Roll",
    strain: 10,
    thc: 1889,
    cbd: 10,
    lab_test: 53
    },
```

This endpoint retrieves all products.

### HTTP Request

`GET http://kushy.net/api/v3/products/`

### Query Parameters

Parameter | Default | Description
--------- | ------- | -----------
filters | null | Set filter parameters (see filter section for more info)
limit | 20 | Sets the number of items retrieved. Max is 20.

## Get a Specific Product

```shell
  curl "http://kushy.net/api/v3/products/:id"
```

This endpoint retrieves a specific product.

### HTTP Request

`GET http://kushy.net/api/v3/products/<ID>`

### URL Parameters

Parameter | Description
--------- | -----------
ID | The ID of the product to retrieve

## Delete a Specific Product


```shell
curl "http://kushy.net/api/v3/products/420"
  -X DELETE
  -H "Authorization: API_KEY"
```

This endpoint deletes a specific product.

### HTTP Request

`DELETE http://kushy.net/api/v3/products/<ID>`

### URL Parameters

Parameter | Description
--------- | -----------
ID | The ID of the product to delete