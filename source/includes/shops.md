# Shops

## Get All Shops

```shell
  curl "http://kushy.net/api/v3/shops/"
```

```javascript
var api = 'http://kushy.net/api/v3/shops/';
var xmlhttp;
xmlhttp = new XMLHttpRequest();
xmlhttp.onreadystatechange = function(){
    if (xmlhttp.readyState == 4 && xmlhttp.status == 200){
      shops = JSON.parse(this.responseText);
      console.log(shops);
    }
}
xmlhttp.open("GET", api, true);
xmlhttp.send();
```

```php
<?php
$api = 'http://api.kushy.net/api/1.1/tables/shops/';
$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => $api,
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "GET",
  CURLOPT_HTTPHEADER => array(
    "cache-control: no-cache"
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);
$response = json_decode($response, true);
echo 'Shop Name'. $response['data'][0]['name'];
?>
```

> The above command returns JSON structured like this:

```json
[
  meta: {
    table: "shops",
    type: "collection",
    total: 20
  },
  data: [
    {
        id: 911,
        status: 2,
        sort: 0,
        name: "ShowGrow Ramona",
        slug: "showgrow-ramona/",
        featured_image: "http://kushy.net/wp-content/uploads/2016/12/square_logo.jpg",
        avatar: "http://kushy.net/wp-content/uploads/2016/12/square_logo.jpg",
        images: null,
        gallery: null,
        description: "",
        lat: 33.043339,
        lng: -116.905479,
        address: "",
        city: "Ramona",
        state: "CA",
        postcode: "92065",
        country: "US",
        hours: "",
        coupons: null,
        deals: null,
        rating: null,
        tags: null,
        twitter: "",
        facebook: "",
        instagram: "",
        tumblr: "",
        googleplus: "",
        type: "Medical Marijuana Dispensary",
    }]
  }
]
```

This endpoint retrieves all shops.

### HTTP Request

`GET http://kushy.net/api/v3/shops/?<FILTER><LIMIT>`

### Query Parameters

Parameter | Default | Description
--------- | ------- | -----------
filters | null | Set filter parameters (see filter section for more info)
region | null | Set region parameters (see region section below)
limit | 20 | Sets the number of items retrieved. Max is 20.

## Get a Specific Shop

```shell
  curl "http://kushy.net/api/v3/shops/:id"
```

This endpoint retrieves a specific shop.

### HTTP Request

`GET http://kushy.net/api/v3/shops/<ID>`

### URL Parameters

Parameter | Description
--------- | -----------
ID | The ID of the shop to retrieve

## Delete a Specific Shop


```shell
curl "http://kushy.net/api/v3/shops/420"
  -X DELETE
  -H "Authorization: API_KEY"
```

This endpoint deletes a specific shop.

### HTTP Request

`DELETE http://kushy.net/api/v3/shops/<ID>`

### URL Parameters

Parameter | Description
--------- | -----------
ID | The ID of the shop to delete

## Get Shops by Region

```shell
  curl "http://kushy.net/api/v3/shops/<Region>"
```

This endpoint retrieves shops within a circular region of the coordinates provided (using the Haversine formula).

### HTTP Request

`GET http://kushy.net/api/v3/shops/?region[geo][lat]=33.719657&region[geo][lng]=-117.852899&region[geo][radius]=20`

### URL Parameters

Parameter | Description
--------- | -----------
Region | Query string object with geo info (lat, lng, and radius)

In object form:

`region: {
    geo: {
        lat: 33,
        lng: -117,
        radius: 20
        }
    }`

