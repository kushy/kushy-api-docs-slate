---
title: Kushy API Documentation

language_tabs: # must be one of https://git.io/vQNgJ
  - shell: Bash
  - javascript: JavaScript
  - php: PHP
  - json: JSON

toc_footers:
  - <a href='#'>Sign Up for a Developer Key</a>
  - <a href='https://github.com/lord/slate'>Documentation Powered by Slate</a>

includes:
  - auth
  - strains
  - products
  - shops
  - filters
  - errors

search: true
---

# Introduction

Welcome to the Kushy API! You can use our API to access Kushy API endpoints, which can get information on various cannabis strains, products, shops, and brands in our database.

We have language bindings in Javascript and PHP! You can view code examples in the dark area to the right, and you can switch the programming language of the examples with the tabs in the top right. Find example apps and more [on our Github](https://github.com/kushyapp/).
