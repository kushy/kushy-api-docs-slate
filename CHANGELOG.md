# Changelog

## v1.1.3 - January 17, 2018

**Fixed filters info. Removed template filler.**

## v1.1.2 - January 17, 2018

Actually modified CSS.

## v1.1.1 - January 17, 2018

**Filters + CSS**

* Changed status updates CSS.
* Changed API URL from subdomain to root domain.
* Added filters section

## v1.1 - January 6, 2018

**Docs created! Started CSS styling!**

* Changed API URL to new /v3/ format.

## v1.0 - November 28, 2017

**Docs created! Started CSS styling!**

* Had an issue with neotiger gem not installing. Needed to install XCode CLI (xcode--select).
* Edit and add new files in the `./includes/` folder, then include in the `index.html.md` file.

