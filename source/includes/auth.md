# Authentication

> To authorize, use this code:

```shell
  curl "http://kushy.net/api/v3/strains/"
```


```php
<?php
$api = 'http://kushy.net/api/v3/strains/';
$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => $api,
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "GET",
  CURLOPT_HTTPHEADER => array(
    "cache-control: no-cache"
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);
$response = json_decode($response, true);
print_r($response);
?>
```

```javascript
var api = 'http://kushy.net/api/v3/strains/';
var xmlhttp;
xmlhttp = new XMLHttpRequest();
xmlhttp.onreadystatechange = function(){
    if (xmlhttp.readyState == 4 && xmlhttp.status == 200){
      strains = JSON.parse(this.responseText);
      console.log(strains);
    }
}
xmlhttp.open("GET", api, true);
xmlhttp.send();
```

Kushy allows for GET requests from any public request, just request your data and go!

As a verified developer you'll gain access to API keys to allow POST, PUT, and DELETE requests. We are currently not accepting new applications for API keys, please check back soon!

<aside class="notice">
Looking for our data? Please don't scrape us - <a href="http://github.com/user/kushyapp">download our database on Github</a>
</aside>