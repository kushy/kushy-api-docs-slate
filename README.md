# Kushy API Docs - Slate



## Getting Started

### Dev Server

1. Fork this repository on GitHub.
2. Clone *your forked repository* (not our original one) to your hard drive with `git clone https://github.com/YOURUSERNAME/slate.git`
3. `cd slate`
4. Initialize and start Slate. You can either do this locally, or with Vagrant:

```shell
# either run this to run locally
bundle install
bundle exec middleman server

# OR run this to run with vagrant
vagrant up
```

You can now see the docs at http://localhost:4567. Whoa! That was fast!

### Production Build

Middleman will build your website to the build directory of your project, and you can copy those static HTML files to the server of your choice.

```shell
bundle exec middleman build --clean
```

Do not run this script on production server, Middleman is not meant for prod. Instead, copy static files to server somehow using CI. Consider [middleman-deploy](https://github.com/middleman-contrib/middleman-deploy).